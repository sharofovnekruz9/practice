console.log("Задача 1");
const password = "1234-";
((password.includes("-") || password.includes("_")) && password.length >= 4) ? console.log("Пароль надёжный") : console.log("Пароль недостаточно надёжный");

console.log("Задача 2");
const userName = "ВаДя";
const userSurname = "ПеТров";

const modifyName = userName[0].toUpperCase() + userName.substring(1).toLowerCase();
const modifySurname = userSurname[0].toUpperCase() + userSurname.substring(1).toLowerCase();

console.log(modifyName + " " + modifySurname);

((userName == modifyName) && (userSurname == modifySurname)) ? console.log("Имя осталось без изменений") : console.log("Имя было преобразовано");


console.log("Задача 3");
let num = 8;
(num % 2 == 0) ? console.log("Число чётное") : console.log("Число нечётное");