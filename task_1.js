function createStudentCard(name, age) {
    const div = document.createElement('div');
    const h2 = document.createElement('h2');
    const span = document.createElement('span');

    document.body.append(div);
    div.append(h2);
    div.append(span);

    h2.textContent = name;
    span.textContent = `Возраст: ${age} лет`;
}


createStudentCard('Игорь', 17);