console.log("Задача 1");
function getAge(year) {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    return (currentYear - year);
}
console.log(getAge(1998));

console.log("Задача 2");
function filter(whiteList, blackList) {
    return whiteList.filter(e => !blackList.includes(e));
}
// Массив с почтовыми адресами:
let whiteList = ['my-email@gmail.ru', 'jsfunc@mail.ru', 'annavkmail@vk.ru', 'fullname@skill.ru', 'goodday@day.ru'];
// Массив с почтовыми адресами в чёрном списке:
let blackList = ['jsfunc@mail.ru', 'goodday@day.ru'];
// Вызов созданной функции:
let result = filter(whiteList, blackList);
console.log(result);

console.log("Задача 3")
function arrSort(arr) {
    return console.log(arr.sort());
}
let array = [0, 1, 3, 5, -1]
arrSort(array);