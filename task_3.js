const allStudents = [
    { name: 'Валя', age: 11 },
    { name: 'Таня', age: 24 },
    { name: 'Рома', age: 21 },
    { name: 'Надя', age: 34 },
    { name: 'Антон', age: 7 }
]

function createStudentList(listArr) {
    const ul = document.createElement('ul');
    document.body.append(ul);
    listArr.forEach(student => {
        const li = document.createElement('li');
        const h2 = document.createElement('h2');
        const span = document.createElement('span');

        ul.append(li);

        li.appendChild(h2);
        li.appendChild(span);

        h2.textContent = student.name;
        span.textContent = `Возраст: ${student.age} лет`;
    })
}

createStudentList(allStudents);