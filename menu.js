// Массив с блюдами
const menuItems = [
    {
        name: "Борщ",
        category: "Супы",
        price: 150
    },
    {
        name: "Компот",
        category: "Напитки",
        price: 50
    },
    {
        name: "Стейк",
        category: "Мясо",
        price: 500
    },
    {
        name: "Оливье",
        category: "Салаты",
        price: 200
    },
    {
        name: "Чай",
        category: "Напитки",
        price: 70
    }
];

// Функция для отображения меню
function showMenu(category = "") {
    const menuItemsEl = document.getElementById('menuItems');
    menuItemsEl.innerHTML = "";

    const filteredItems = category ? menuItems.filter(item => item.category === category) : menuItems;

    for (let item of filteredItems) {
        const menuItemEl = document.createElement('div');
        menuItemEl.classList.add('menu-item');
        menuItemEl.innerHTML = 
            <h3>${item.name}</h3>
            <p>Категория: ${item.category}</p>
            <p>Цена: ${item.price} руб.</p>
            <button onclick="addToCart('${item.name}')">Добавить в корзину</button>
;
        menuItemsEl.appendChild(menuItemEl);
    }
}

// Функция для добавления товара в корзину
function addToCart(itemName) {
    const cartItemsEl = document.getElementById('cartItems');
    const totalEl = document.getElementById('total');

    // Проверяем, есть ли уже такой товар в корзине
    const existingItem = cart.find(item => item.name === itemName);

    if (existingItem) {
        existingItem.quantity++;
    } else {
        cart.push({
            name: itemName,
            quantity: 1
        });
    }

    cartItemsEl.innerHTML = "";
    totalEl.innerHTML = "";

    let totalPrice = 0;
    for (let item of cart) {
        const cartItemEl = document.createElement('li');
        cartItemEl.innerText = ${item.name} (x${item.quantity});
        cartItemsEl.appendChild(cartItemEl);
        totalPrice += menuItems.find(i => i.name === item.name).price * item.quantity;
    }

    totalEl.innerText = Общая сумма: ${totalPrice} руб.;
    saveCartToLocalStorage();
}

// Функция для сохранения корзины в localStorage
function saveCartToLocalStorage() {
    localStorage.setItem('cart', JSON.stringify(cart));
}

// Проверяем, есть ли сохраненная корзина в localStorage
let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];

// Отображаем меню при загрузке страницы
showMenu();

// Обработчик изменения категории
const categorySelect = document.getElementById('category');
categorySelect.addEventListener('change', () => {
    showMenu(categorySelect.value);
});

// Обработчик кнопки "Оплатить"
const checkoutBtn = document.getElementById('checkout');
checkoutBtn.addEventListener('click', () => {
    alert('Оплата успешно выполнена!');
    cart = [];
    localStorage.removeItem('cart');
    showMenu();
    document.getElementById('cartItems').innerHTML = "";
    document.getElementById('total').innerHTML = "";
});