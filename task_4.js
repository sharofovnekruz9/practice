const btn = document.querySelector('.btn');

const allStudents = [
    { name: 'Валя', age: 11 },
    { name: 'Таня', age: 24 },
    { name: 'Рома', age: 21 },
    { name: 'Надя', age: 34 },
    { name: 'Антон', age: 7 }
]
function createStudentsList(listArr) {
    const ul = document.createElement('ul');
    document.body.append(ul);
    for (let person = 0; person < listArr.length; person++) {
        const h2 = document.createElement('h2');
        const span = document.createElement('span');
        const li = document.createElement('li');

        ul.append(li);

        li.appendChild(h2);
        li.appendChild(span);
        h2.textContent = listArr[person].name;
        span.textContent = `Возраст: ${listArr[person].age} лет`;
    }
}


btn.addEventListener('click', () => createStudentsList(allStudents));